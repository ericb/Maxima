# Résolution numérique d'équations différentielles  ordinaires (EDO)
 
**CE PROJET EST À RENDRE LE JEUDI 3 JUIN À MINUIT AU PLUS TARD**


L'objectif de ce projet de mathématiques, est de se familiariser avec les méthodes numériques utilisées en physique pour simuler une trajectoire ou un comportement lorsque le modèle utilisé fait appel à la résolution d'une équation du premier ou du deuxième ordre à coefficients constants. 
Exemple : on parlera de la méthode d'Euler explicite au premier ordre, et on utilisera l'algorithme associé pour réaliser la simulation.

Travail demandé

## Partie 1 : étude théorique 

- Définir le problème de Cauchy, et donner le théorème de Cauchy-Lipschitz et la condition de Lipschitz. Discuter de leurs utilités respectives.
- Pour la méthode d'Euler (explicite, à l'ordre 1) et Runge Kutta (ordre 2), apporter la preuve de leur convergence lorsqu'elles sont appliquées au problème de Cauchy.
- Comparer leurs vitesses de convergence, et leurs précisions respectives.

## Partie 2 : mise en œuvre 

Proposer un algorithme, pour chacune des méthodes qui suivent :
- méthode d'Euler au premier ordre
- méthode d'Euler au 2ème ordre
- méthode de Runge Kutta au 2ème ordre
- Méthode de Runge Kutta au 4ème ordre

## Partie 3 : étude de la méthode de Verlet

- Présenter cette méthode, en précisant ce qu'elle apporte
- montrer qu'elle permet d'obtenir une solution unique au problème de Cauchy.
- de préférence avec Maxima (ou Python pour ceux qui le souhaitent), écrire un programme permettant de visualiser la courbe obtenue sous forme d'une animation. 
- illustrer cette méthode avec un exemple de type problème à deux corps.
 
Ce qui est attendu 

1. un journal hebdomadaire d'activité par groupe ;  (le barème en tient compte: régularité, planning tenu, etc)

2. Un rapport final (un par groupe), qui ne devra pas excéder 25 pages. Celui-ci sera sous forme de document numérique (format odt type OpenOffice, sinon .pdf ) ;

3. En plus du rapport, un ou plusieurs programme(s) permettant de valider l'étude. Le code fourni sera délivré sous forme de fichier .mac, et respectera les Coding Guidelines données dans l'introduction à la programmation Maxima (si le format .mac est choisi).

## Exemple de problèmes pouvant être modélisés (liste non exhaustive)avec les méthodes étudiées :

### EDO d'ordre 1 :

- charge d'un circuit RC ou d'un circuit RL ;
- décharge d'un circuit RC ou d'un circuit RL ;
- étude du pendule simple ;
- pendule pesant ;
- étude de la chute libre ;
- étude du ballon sonde ;
- saut en parachute ;
- étude d'une trajectoire (en polaires) : satellite etc (problème à 2 corps) ;
- chute libre d'une bille dans un fluide ( frottements du type  ou  ) ;
- mouvement d'un projectile avec frottements.

### EDO d'ordre 2 :

- oscillations mécaniques amorties (masse+ressort+amortisseur);
- [modélisation du mouvement d'un électron](https://framagit.org/ericb/documents/-/tree/master/Informatique/Python/Euler/second_ordre)
- étude d'un régime transitoire dans le cadre d'oscillations forcées ;
- étude du démarrage d'un oscillateur.


### Documents supports suggérés :

Tableur avec exemples fonctionnels permettant de tester :

- [exemples réalisés à l'aide d'un tableur](https://framagit.org/ericb/documents/-/blob/master/Physique/Mecanique_classique/TD/Euler/Euler_exemples.ods)

- [Présentation des méthodes vues en TP](https://framagit.org/ericb/documents/-/blob/master/Physique/Mecanique_classique/TD/Euler/Euler_ordre1_NSI.pdf)

- [Documents complémentaires sur les propriétés des ellipses](https://framagit.org/ericb/documents/-/blob/master/Physique/Mecanique_classique/divers/Mecanique_du_point_Ellipses_Binet.pdf)

- + tout document servant de support pour  la simulation du problème à 2 corps.

### **BIBLIOGRAPHIE**

- [1] Résolution numérique des équations différentielles ordinaires (EDO), Jacques Lefrère UE MU4PY209.

- [2] [**Wikipedia Loup Verlet**](https://fr.wikipedia.org/wiki/Loup_Verlet)

- [3] [**Méthode de Verlet, site Femto physique de Jimmy Roussel**](https://femto-physique.fr/omp/methode-de-verlet.php)  (consultation : 11/04/2021)

- [4] Méthodes d’Euler, méthode de Verlet Judicaël Courant 28 août 2016 Cours d'option MPSI, Lycée La Martinière Montplaisir

- [5] [**Cours de l'Université de Sherbrooke**](https://www.physique.usherbrooke.ca/pages/sites/default/files/PHQ404_0.pdf)

- [6] **Hubert Baty. Approche numérique à l’usage du physicien pour résoudre les équations différentielles ordinaires**. II. Cas des oscillateurs harmonique/an-harmonique et du pendule non linéaire. Licence. France. 2017.  cel-01959896v2

- [7] [**Peter Young. The leapfrog method and other “symplectic” algorithms for integrating newton’s laws of motion.**](http://young.physics.ucsc.edu/115/leapfrog.pdf), apr 2014. Notes du cours Computational Physics.

- [8] [**Rappels à propos de la méthode d'Euler**](https://pauvre.org/~ccharign/optionMP/05Euler/rappelsEuler.pdf)

Pour s'entraîner :

- [9] [**Sujet Centrale Supélec 2015 (MP,PC,PSI,TSI**)](https://www.concours-centrale-supelec.fr/CentraleSupelec/2015/Multi/sujets/2014-030.pdf)

