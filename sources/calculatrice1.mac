/*
  Copyright Eric Bachard / Mai 2006
  License : GPL v2
  see : http://www.gnu.org/licenses/gpl-2.0.html */
/*
   calculatrice.mac
   fonctions utiles pour plein de choses
   - calcul de la valeur d'une fonction pour x donné
   - calcul de la valeur d'une fonction de 2 variables (x,y)   pour x et y donnés
     avec la méthode des rectangles
   - calcul de la valeur d'une fonction de 3 variables (x,y,z) pour x, y et z donnés
   - calcul approchée de l'aire sous une fonction, entre 2 points,

   Ce programme est sous Licence GPL v2. Il est vivement recommandé de faire des sauvegardes avant de l'utiliser.
 */

/* utile pour la saisie d'une fonction d'une seule variable */
f_x(g,x1):=block
(
  [x],
  /*
     G(x) est le nom interne de f_x(x) 
     si on reutilise le nom global, on ne peut faire qu'un calcul
     car la fonction devient une constante et ne peut plus être 
     modifiée i.e. recalculée.
   */
  G(x):=g,
  define( G(x),g ),
  print ("f_x(x) = ",G(x)),
  print("f_x(",x1,") vaut :",float(G(x1)))
);

t_xy(v,x1,y1):=block
(
  E(x,y):=v,
  define(E(x,y),v),
  print("t(",x1,",",y1,") =",float(E(x1,y1)))
);

k_xy(w,x2,y2):=block
(
  [z,t],
  J(z,t):=w,
  define(J(z,t),w),
  print("k(",x2,",",y2,") =",float(J(x2,y2)))
);




/* utile pour la saisie d'une fonction de 2 variables */
f_xy(h,x1 ,y1):=block
(
  [x, y],
  /* H(x,y) est le nom interne de f_xy(x,y) */
  H(x, y):=h,
  define( H(x, y),h ),
  print ("f_xy(x,y) = ",H(x, y)),
  print("f_xy(",x1,",",y1,") vaut :",float(H(x1,y1)))
);

/* utile pour la saisie d'une fonction de 3 variables */
f_xyz(u,x1,y1,z1):=block
(
  [x,y,z],
  /* U(x,y,z) est le nom interne de f_xyz(x,y,z) */
  U(x,y,z):=u,
  define( U(x,y,z),u ),
  print ("f_xyz(x,y,z) = ",U(x,y,z)),
  print("f_xyz(",x1,",",y1,",",z1,") vaut :",float(U(x1,y1,z1)))
);

/* calcul rapide d'une aire par la méthode des rectangles
   avec la fonction à intégrer passée en paramètres
 */
rectangles(f,a,b,n):=block
(
    [aire,pas],
    G(x)::=f,
    define( G(x),f ),
    aire:0,
    pas:(b-a)/n,
    for valeur:a step pas thru (b-pas) do
    (
      aire:aire + pas*G(valeur),
      float(aire)
    ),
return (float(aire)));

/* Évaluation rapide d'une aire par la méthode des rectangles
   avec la fonction à intégrer passée en paramètres
 */
trapezes(f,a,b,n):=block
(
    [aire,pas],
    G(x)::=f,
    define( G(x),f ),
    aire:0,
    pas:(b-a)/n,
    for valeur:a step pas thru (b-pas) do
    (
        aire:pas*( (G(a)+G(b))/2 + sum( G( (a)+(k*pas) ) ,k,1,n-1) )
    ),
return(float(aire)));

dicho(f,borne_inf,borne_sup):=block
(
  [epsilon,result,milieu,compteur],
  G(x)::=f,
  define( G(x),f ),
  epsilon:0.00001,
  compteur:0,
  min:borne_inf,
  max:borne_sup,
  milieu: (min+max)/2,
  while (abs(G(milieu)) > epsilon)
  do
  (
     compteur:compteur+1,
     result:milieu,
     if ( G(min) * G(milieu) < 0) then max:milieu

     else if ( G(min) * G(milieu) > 0 ) then min:milieu,

     milieu: (max +min) / 2
  ),
  print("La racine de ",G(x),"= 0 entre 0 et 1 vaut", float(milieu)," et a été obtenue en",compteur,"itérations")
);


dichotomie():=block
(
  [f, borne_sup, borne_inf, epsilon,result,milieu,compteur],
  f:read("Entrer la fonction à étudier :"),
  G(x)::=f,
  define( G(x),f ),
  borne_inf:read("Entrer la borne inférieure de l'intervalle :"),
  borne_sup:read("Entrer la borne supérieure de l'intervalle :"),
  epsilon:read("Entrer epsilon. Valeur conseillée entre 1e-6 et 1e-2 :"),
  compteur:0,
  min:borne_inf,
  max:borne_sup,
  milieu: (min+max)/2,
  while (abs(G(milieu)) > epsilon)
  do
  (
      compteur:compteur+1,
      result:milieu,

      if ( G(min) * G(milieu) < 0)
        then max:milieu

      else if ( G(min) * G(milieu) > 0 )
        then min:milieu,

      milieu: (max +min) / 2
  ),
  print("La racine de ",G(x),"= 0 entre 0 et 1 vaut", float(milieu)," et a été obtenue en",compteur,"itérations")
);
